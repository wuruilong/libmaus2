/*
    libmaus2
    Copyright (C) 2009-2016 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_AIO_MEMORYBACKEDOUTPUTSTREAMBUFFER_HPP)
#define LIBMAUS2_AIO_MEMORYBACKEDOUTPUTSTREAMBUFFER_HPP

#include <libmaus2/LibMausConfig.hpp>

#include <ostream>
#include <libmaus2/autoarray/AutoArray.hpp>
#include <libmaus2/aio/OutputStreamInstance.hpp>

namespace libmaus2
{
	namespace aio
	{
		struct MemoryBackedOutputStreamBuffer : public ::std::streambuf
		{
			private:
			std::string const backfn;
			libmaus2::autoarray::AutoArray<char> A;
			uint64_t o;
			libmaus2::aio::OutputStreamInstance::unique_ptr_type backOSI;

			public:
			MemoryBackedOutputStreamBuffer(
				std::string const & rbackfn,
				uint64_t const rn
			)
			: backfn(rbackfn), A(rn,false), o(0)
			{
				setp(nullptr,nullptr);
			}

			~MemoryBackedOutputStreamBuffer()
			{
			}

			std::string getURL()
			{
				if ( backOSI )
				{
					backOSI->flush();
					if ( ! *backOSI )
					{
						libmaus2::exception::LibMausException lme;
						lme.getStream() << "[E] MemoryBackedOutputStreamBuffer::getURL: failed to flush backing file" << std::endl;
						lme.finish();
						throw lme;
					}
					backOSI.reset();
					return backfn;
				}
				else
				{
					std::string const memfn = std::string("mem:") + backfn;
					libmaus2::aio::OutputStreamInstance::unique_ptr_type pOSI(new libmaus2::aio::OutputStreamInstance(memfn));
					pOSI->write(A.begin(),o);
					pOSI->flush();
					if ( ! *pOSI )
					{
						libmaus2::exception::LibMausException lme;
						lme.getStream() << "[E] MemoryBackedOutputStreamBuffer::getURL: failed to flush memory backing file" << std::endl;
						lme.finish();
						throw lme;
					}
					pOSI.reset();
					return memfn;
				}
			}

			int_type overflow(int_type c = traits_type::eof())
			{
				if ( c != traits_type::eof() )
				{
					if ( o < A.size() )
					{
						A[o++] = c;

						if ( o == A.size() )
						{
							libmaus2::aio::OutputStreamInstance::unique_ptr_type tbackOSI(
								new libmaus2::aio::OutputStreamInstance(backfn)
							);
							backOSI = std::move(tbackOSI);
							backOSI->write(A.begin(),A.size());
							if ( ! *backOSI )
								return traits_type::eof();
						}
					}
					else
					{
						backOSI->put(c);
						if ( ! backOSI )
							return traits_type::eof();
					}
				}

				return c;
			}

			int sync()
			{
				return 0; // no error, -1 for error
			}
		};
	}
}
#endif
