/*
    libmaus2
    Copyright (C) 2009-2014 German Tischler
    Copyright (C) 2011-2014 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if !defined(LIBMAUS2_AIO_SKIPCONCATINPUTSTREAMBUFFER_HPP)
#define LIBMAUS2_AIO_SKIPCONCATINPUTSTREAMBUFFER_HPP

#include <streambuf>
#include <istream>
#include <libmaus2/autoarray/AutoArray.hpp>
#include <libmaus2/aio/InputStreamInstance.hpp>

namespace libmaus2
{
	namespace aio
	{
		struct SkipConcatInputStreamBuffer : public ::std::streambuf
		{
			public:
			typedef SkipConcatInputStreamBuffer this_type;
			typedef std::unique_ptr<this_type> unique_ptr_type;
			typedef std::shared_ptr<this_type> shared_ptr_type;

			private:
			std::vector< std::pair<std::string,uint64_t> > const filenames;
			std::vector< std::pair<std::string,uint64_t> >::const_iterator filenames_ita;
			uint64_t const blocksize;
			uint64_t const putbackspace;
			::libmaus2::autoarray::AutoArray<char> buffer;
			libmaus2::aio::InputStreamInstance::unique_ptr_type Pin;
			uint64_t symsread;

			SkipConcatInputStreamBuffer(SkipConcatInputStreamBuffer const &);
			SkipConcatInputStreamBuffer & operator=(SkipConcatInputStreamBuffer &);

			std::streamsize readblock(char * p, std::streamsize s)
			{
				std::streamsize r = 0;

				while ( s )
				{
					// no stream open?
					if ( ! Pin )
					{
						// open next file if we have any
						if ( filenames_ita != filenames.end() )
						{
							std::pair<std::string,uint64_t> const P = *(filenames_ita++);
							libmaus2::aio::InputStreamInstance::unique_ptr_type Tin(new libmaus2::aio::InputStreamInstance(P.first));
							Pin = std::move(Tin);
							if ( P.second )
								Pin->ignore(P.second);
						}
						// no more files, set s to zero
						else
						{
							s = 0;
						}
					}
					// if file is open
					if ( Pin )
					{
						Pin->read(p,s);
						std::streamsize const t = Pin->gcount();
						r += t;
						s -= t;
						p += t;
						if ( ! t )
						{
							Pin.reset();
						}
					}
				}

				return r;
			}

			public:
			SkipConcatInputStreamBuffer(
				std::vector< std::pair<std::string,uint64_t> > const & rfilenames,
				int64_t const rblocksize,
				uint64_t const rputbackspace = 0
			)
			:
			  filenames(rfilenames),
			  filenames_ita(filenames.begin()),
			  blocksize(rblocksize),
			  putbackspace(rputbackspace),
			  buffer(putbackspace + blocksize,false),
			  Pin(),
			  symsread(0)
			{
				setg(buffer.end(),buffer.end(),buffer.end());
			}

			private:
			// gptr as unsigned pointer
			uint8_t const * uptr() const
			{
				return reinterpret_cast<uint8_t const *>(gptr());
			}

			int_type underflow()
			{
				// if there is still data, then return it
				if ( gptr() < egptr() )
					return static_cast<int_type>(*uptr());

				assert ( gptr() == egptr() );

				// number of bytes for putback buffer
				uint64_t const putbackcopy = std::min(
					static_cast<uint64_t>(gptr() - eback()),
					putbackspace
				);
				// copy bytes
				std::copy(
					gptr()-putbackcopy,
					gptr(),
					buffer.begin() + putbackspace - putbackcopy
				);

				// load data
				uint64_t const uncompressedsize = readblock(buffer.begin()+putbackspace,buffer.size()-putbackspace);

				// set buffer pointers
				setg(
					buffer.begin()+putbackspace-putbackcopy,
					buffer.begin()+putbackspace,
					buffer.begin()+putbackspace+uncompressedsize);

				symsread += uncompressedsize;

				if ( uncompressedsize )
					return static_cast<int_type>(*uptr());
				else
					return traits_type::eof();
			}
		};
	}
}
#endif
