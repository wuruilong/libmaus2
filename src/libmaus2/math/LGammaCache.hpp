/*
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_MATH_LGAMMACACHE_HPP)
#define LIBMAUS2_MATH_LGAMMACACHE_HPP

#include <cmath>
#include <vector>
#include <libmaus2/types/types.hpp>

namespace libmaus2
{
	namespace math
	{
		struct LGammaCache
		{
			std::vector<double> lcache;

			double getLGamma(uint64_t const i)
			{
				while ( !(i < lcache.size()) )
				{
					uint64_t const j = lcache.size();
					double const v = ::std::lgamma(j);
					lcache.push_back(v);
				}
				return lcache[i];
			}
		};
	}
}
#endif
