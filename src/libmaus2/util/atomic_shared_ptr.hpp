/*
    libmaus2
    Copyright (C) 2021 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_UTIL_ATOMICSHAREDPTR_HPP)
#define LIBMAUS2_UTIL_ATOMICSHAREDPTR_HPP

#include <libmaus2/LibMausConfig.hpp>

#if defined(LIBMAUS2_HAVE_BOOST_SMART_PTR_ATOMIC_SHARED_PTR)
#include <boost/smart_ptr/atomic_shared_ptr.hpp>
#endif

#include <memory>
#include <atomic>

namespace libmaus2
{
	namespace util
	{
#if defined(LIBMAUS2_USE_STD_ATOMIC_SHARED_PTR)
		template<typename type> using shared_ptr = std::shared_ptr<type>;
		template<typename type> using atomic_shared_ptr = std::atomic< std::shared_ptr<type> >;
#elif defined(LIBMAUS2_USE_BOOST_ATOMIC_SHARED_PTR)
		template<typename type> using shared_ptr = boost::shared_ptr<type>;
		template<typename type> using atomic_shared_ptr = boost::atomic_shared_ptr< type >;
#else
#error "No support for std::atomic<std::shared_ptr> and boost::atomic_shared_ptr"
#endif
	}
}

#endif // LIBMAUS2_UTIL_ATOMICSHAREDPTR_HPP
