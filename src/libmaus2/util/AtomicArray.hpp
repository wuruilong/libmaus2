/*
    libmaus2
    Copyright (C) 2021 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_UTIL_ATOMICARRAY_HPP)
#define LIBMAUS2_UTIL_ATOMICARRAY_HPP

#if defined(_OPENMP)
#include <omp.h>
#endif

#include <atomic>
#include <new>
#include <libmaus2/util/NumberSerialisation.hpp>
#include <libmaus2/util/atomic_shared_ptr.hpp>

namespace libmaus2
{
	namespace util
	{
		template<typename type>
		struct AtomicArray
		{
			private:
			std::size_t threads;
			std::size_t n;
			std::shared_ptr<char[]> c_A;
			std::atomic<type> * p_A;
			std::atomic<bool> initRequired;

			AtomicArray() = delete;
			AtomicArray<type>(AtomicArray<type> const & O) = delete;
			AtomicArray<type> & operator=(AtomicArray<type> const & O) = delete;

			void destroyArray()
			{
				typedef typename std::atomic<type> a_type;
				for ( std::size_t i = 0; i < n; ++i )
					p_A[i].~a_type();
			}

			void allocateArray(std::size_t const r_n)
			{
				std::shared_ptr<char[]> t_c_A(new char[r_n * sizeof(std::atomic<type>)]);
				c_A = t_c_A;
				n = r_n;
				p_A = reinterpret_cast<std::atomic<type>*>(c_A.get());
			}


			void setupArray(std::size_t r_n, type const & t)
			{
				allocateArray(r_n);
				initArray(0,r_n,t);
			}

			AtomicArray(std::size_t const r_n)
			: threads(1), n(r_n), c_A(), p_A(nullptr), initRequired(true)
			{
				allocateArray(r_n);
			}

			public:
			AtomicArray(std::size_t const r_n, type const & t, std::size_t const r_threads = 1)
			: threads(r_threads), n(0), c_A(), p_A(nullptr), initRequired(false)
			{
				setupArray(r_n, t);
			}

			template<typename func_type>
			AtomicArray(std::istream & in, func_type func, std::size_t const r_threads = 1)
			: threads(r_threads), initRequired(false)
			{
				std::size_t const r_n = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
				allocateArray(r_n);
				for ( std::size_t i = 0; i < r_n; ++i )
				{
					type const t(func(in));
					new(p_A+i) std::atomic<type>(t);
				}
			}

			~AtomicArray()
			{
				destroyArray();
			}

			bool getInitRequired() const
			{
				return initRequired.load();
			}

			void clearInitRequired()
			{
				initRequired.store(false);
			}

			static libmaus2::util::shared_ptr< AtomicArray<type> > allocateUnitiliazed(std::size_t const r_n)
			{
				libmaus2::util::shared_ptr< AtomicArray<type> > sptr(new AtomicArray<type>(r_n));
				return sptr;
			}

			void initArray(std::size_t const r_from, std::size_t r_to, type const & t)
			{
				#if defined(_OPENMP)
				if ( threads <= 1 )
				#endif
					for ( std::size_t i = r_from; i < r_to; ++i )
						new(p_A+i) std::atomic<type>(t);
				#if defined(_OPENMP)
				else
				{
					#pragma omp parallel for num_threads(threads)
					for ( std::size_t i = r_from; i < r_to; ++i )
						new(p_A+i) std::atomic<type>(t);
				}
				#endif
			}


			template<typename func_type>
			void serialise(std::ostream & ostr, func_type func) const {
				libmaus2::util::NumberSerialisation::serialiseNumber(ostr,n);
				for ( auto it = cbegin(); it != cend(); ++it )
					func(ostr,it->load());
			}

			std::size_t size() const
			{
				return n;
			}

			std::atomic<type> & at(std::size_t const i)
			{
				if ( i < n )
					return p_A[i];
				else
				{
					std::ostringstream ostr;
					ostr << "AtomicArray::at(" << i << ") index is out of range [0," << n << ")";
					throw std::out_of_range(ostr.str());
				}
			}

			std::atomic<type> const & at(std::size_t const i) const
			{
				if ( i < n )
					return p_A[i];
				else
				{
					std::ostringstream ostr;
					ostr << "AtomicArray::at(" << i << ") index is out of range [0," << n << ")";
					throw std::out_of_range(ostr.str());
				}
			}

			std::atomic<type> & operator[](std::size_t const i)
			{
				return p_A[i];
			}

			std::atomic<type> const & operator[](std::size_t const i) const
			{
				return p_A[i];
			}

			std::atomic<type> * begin()
			{
				return p_A;
			}

			std::atomic<type> const * begin() const
			{
				return p_A;
			}

			std::atomic<type> * get()
			{
				return p_A;
			}

			std::atomic<type> const * get() const
			{
				return p_A;
			}

			std::atomic<type> const * cbegin() const
			{
				return p_A;
			}

			std::atomic<type> * end()
			{
				return p_A + n;
			}

			std::atomic<type> const * end() const
			{
				return p_A + n;
			}

			std::atomic<type> const * cend() const
			{
				return p_A + n;
			}
		};
	}
}
#endif
