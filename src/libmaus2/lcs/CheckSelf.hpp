/*
    libmaus2
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_LCS_CHECKSELF_HPP)
#define LIBMAUS2_LCS_CHECKSELF_HPP

namespace libmaus2
{
	namespace lcs
	{
		template<typename it_a, typename it_b, bool const self>
		struct CheckSelf
		{
		};

		template<typename it_a, typename it_b>
		struct CheckSelf<it_a,it_b,false>
		{
			static bool check(it_a, it_b)
			{
				return true;
			}
		};

		template<typename it_a, typename it_b>
		struct CheckSelf<it_a,it_b,true>
		{
			static bool check(it_a a, it_b b)
			{
				return a != b;
			}
		};
	}
}
#endif
