/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_GAMMA_GAMMAGENERICBASE_HPP)
#define LIBMAUS2_GAMMA_GAMMAGENERICBASE_HPP

#include <libmaus2/math/numbits.hpp>

namespace libmaus2
{
	namespace gamma
	{
		template<unsigned int l>
		struct GammaGenericBase
		{
		};

		template<>
		struct GammaGenericBase<1>
		{
			libmaus2::math::NumBits8 const NB;

			GammaGenericBase() : NB() {}

			unsigned int getLength(unsigned int const i) const
			{
				return NB(i);
			}
		};

		template<>
		struct GammaGenericBase<2>
		{
			libmaus2::math::NumBits16 const NB;

			GammaGenericBase() : NB() {}

			unsigned int getLength(unsigned int const i) const
			{
				return NB(i);
			}
		};

		template<>
		struct GammaGenericBase<4>
		{
			libmaus2::math::NumBits32 const NB;

			GammaGenericBase() : NB() {}

			unsigned int getLength(unsigned int const i) const
			{
				return NB(i);
			}
		};

		template<>
		struct GammaGenericBase<8>
		{
			libmaus2::math::NumBits64 const NB;

			GammaGenericBase() : NB() {}

			unsigned int getLength(unsigned int const i) const
			{
				return NB(i);
			}
		};
	}
}
#endif
