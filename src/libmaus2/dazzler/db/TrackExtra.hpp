/*
    libmaus2
    Copyright (C) 2018 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_DAZZLER_DB_TRACKEXTRA_HPP)
#define LIBMAUS2_DAZZLER_DB_TRACKEXTRA_HPP

#include <libmaus2/types/types.hpp>
#include <string>
#include <vector>

namespace libmaus2
{
	namespace dazzler
	{
		namespace db
		{
			struct TrackExtra
			{
				int32_t vtype;
				int32_t nelem;
				int32_t accum;
				int32_t slen;
				std::string name;
				std::vector<uint64_t> value;

				static void putLE(std::ostream & out, uint64_t const u, uint64_t const l)
				{
					for ( uint64_t i = 0; i < l; ++i )
						out.put((u>>(8*i))&0xFF);
				}

				void write(std::ostream & out) const
				{
					putLE(out,vtype,4);
					putLE(out,nelem,4);
					putLE(out,accum,4);
					putLE(out,slen,4);
					out.write(name.c_str(),slen);
					for ( uint64_t i = 0; i < value.size(); ++i )
						putLE(out,value[i],8);
				}

				TrackExtra() {}
				TrackExtra(
					int32_t const rvtype,
					int32_t const rnelem,
					int32_t const raccum,
					int32_t const rslen,
					std::string const & rname,
					std::vector<uint64_t> const & rvalue
				) : vtype(rvtype), nelem(rnelem), accum(raccum), slen(rslen), name(rname), value(rvalue) {}
			};
		}
	}
}
#endif
