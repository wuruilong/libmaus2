/*
    libmaus2
    Copyright (C) 2009-2015 German Tischler
    Copyright (C) 2011-2015 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_BAMBAM_PARALLEL_BAMBLOCKNUMERICALINDEXINGWORKPACKAGE_HPP)
#define LIBMAUS2_BAMBAM_PARALLEL_BAMBLOCKNUMERICALINDEXINGWORKPACKAGE_HPP

#include <libmaus2/bambam/parallel/GenericInputControlCompressionPending.hpp>
#include <libmaus2/bambam/BamNumericalIndexGenerator.hpp>
#include <libmaus2/parallel/SimpleThreadWorkPackage.hpp>

namespace libmaus2
{
	namespace bambam
	{
		namespace parallel
		{
			struct BamBlockNumericalIndexingWorkPackage : public libmaus2::parallel::SimpleThreadWorkPackage
			{
				typedef BamBlockNumericalIndexingWorkPackage this_type;
				typedef std::unique_ptr<this_type> unique_ptr_type;
				typedef std::shared_ptr<this_type> shared_ptr_type;

				libmaus2::bambam::parallel::GenericInputControlCompressionPending GICCP;
				libmaus2::bambam::BamNumericalIndexGenerator * bamnumericalindexgenerator;

				BamBlockNumericalIndexingWorkPackage() : libmaus2::parallel::SimpleThreadWorkPackage() {}
				BamBlockNumericalIndexingWorkPackage(
					uint64_t const rpriority,
					uint64_t const rdispatcherid,
					libmaus2::bambam::parallel::GenericInputControlCompressionPending rGICCP,
					libmaus2::bambam::BamNumericalIndexGenerator * rbamnumericalindexgenerator
				)
				: libmaus2::parallel::SimpleThreadWorkPackage(rpriority,rdispatcherid), GICCP(rGICCP), bamnumericalindexgenerator(rbamnumericalindexgenerator)
				{
				}

				char const * getPackageName() const
				{
					return "BamBlockNumericalIndexingWorkPackage";
				}
			};
		}
	}
}
#endif
