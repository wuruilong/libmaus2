/**
    libmaus2
    Copyright (C) 2009-2021 German Tischler-Höhle
    Copyright (C) 2011-2014 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#if ! defined(LIBMAUS2_SUFFIXSORT_BWTB3M_PREISAADAPTER_HPP)
#define LIBMAUS2_SUFFIXSORT_BWTB3M_PREISAADAPTER_HPP

#include <memory>
#include <libmaus2/aio/ConcatInputStream.hpp>
#include <libmaus2/util/iterator.hpp>
#include <libmaus2/aio/SynchronousGenericInput.hpp>

namespace libmaus2
{
	namespace suffixsort
	{
		namespace bwtb3m
		{
			struct PreIsaAdapter
			{
				typedef PreIsaAdapter this_type;
				typedef std::unique_ptr<this_type> unique_ptr_type;

				mutable libmaus2::aio::ConcatInputStream ISI;
				uint64_t const fs;
				uint64_t const n;

				typedef libmaus2::util::ConstIterator<this_type,std::pair<uint64_t,uint64_t> > const_iterator;

				PreIsaAdapter(std::vector<std::string> const & fn) : ISI(fn), fs(libmaus2::util::GetFileSize::getFileSize(ISI)), n(fs / (2*sizeof(uint64_t)))
				{
					assert ( (fs % (2*sizeof(uint64_t))) == 0 );
				}

				std::pair<uint64_t,uint64_t> get(uint64_t const i) const
				{
					ISI.clear();
					ISI.seekg(i*2*sizeof(uint64_t));
					libmaus2::aio::SynchronousGenericInput<uint64_t> SGI(ISI,2);

					std::pair<uint64_t,uint64_t> P;
					bool ok = SGI.getNext(P.first);
					ok = ok && SGI.getNext(P.second);
					assert ( ok );
					return P;
				}

				std::pair<uint64_t,uint64_t> operator[](uint64_t const i) const
				{
					return get(i);
				}

				const_iterator begin() const
				{
					return const_iterator(this,0);
				}

				const_iterator end() const
				{
					return const_iterator(this,n);
				}
			};
		}
	}
}
#endif
