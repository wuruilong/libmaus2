/*
    libmaus2
    Copyright (C) 2009-2013 German Tischler
    Copyright (C) 2011-2013 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_LZ_BGZFDEFLATEZSTREAMBASE_HPP)
#define LIBMAUS2_LZ_BGZFDEFLATEZSTREAMBASE_HPP

#include <libmaus2/lz/BgzfDeflateHeaderFunctions.hpp>
#include <libmaus2/lz/BgzfDeflateInputBufferBase.hpp>
#include <libmaus2/lz/BgzfDeflateOutputBufferBase.hpp>
#include <libmaus2/lz/BgzfDeflateZStreamBaseFlushInfo.hpp>
#include <libmaus2/lz/DeflateDefaults.hpp>

namespace libmaus2
{
	namespace lz
	{
		struct BgzfDeflateZStreamBase : public BgzfDeflateHeaderFunctions
		{
			typedef BgzfDeflateZStreamBase this_type;
			typedef std::unique_ptr<this_type> unique_ptr_type;
			typedef std::shared_ptr<this_type> shared_ptr_type;

			private:
			#if defined(LIBMAUS2_HAVE_LIBDEFLATE)
			void * compressor;
			#endif

			libmaus2::lz::ZlibInterface::unique_ptr_type zintf;

			unsigned int deflbound;
			int level;

			void deflateinit(int const rlevel);
			void deflatedestroy();
			void resetz();

			// compress block of length len from input pa to output outbuf
			// returns the number of compressed bytes produced
			uint64_t compressBlock(uint8_t * pa, uint64_t const len, uint8_t * outbuf);

			BgzfDeflateZStreamBaseFlushInfo flushBound(
				BgzfDeflateInputBufferBase & in,
				BgzfDeflateOutputBufferBase & out,
				bool const fullflush
			);

			public:
			static uint64_t computeDeflateBound(int const rlevel);

			BgzfDeflateZStreamBase(int const rlevel = libmaus2::lz::DeflateDefaults::getDefaultLevel());
			~BgzfDeflateZStreamBase();

			// flush input buffer into output buffer
			BgzfDeflateZStreamBaseFlushInfo flush(
				BgzfDeflateInputBufferBase & in,
				BgzfDeflateOutputBufferBase & out,
				bool const fullflush
			);

			BgzfDeflateZStreamBaseFlushInfo flush(uint8_t * const pa, uint8_t * const pe, BgzfDeflateOutputBufferBase & out);
			void deflatereinit(int const rlevel = libmaus2::lz::DeflateDefaults::getDefaultLevel());
		};
	}
}
#endif
