/*
    libmaus2
    Copyright (C) 2009-2020 German Tischler-Höhle
    Copyright (C) 2011-2013 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_STDSPINLOCK_HPP)
#define LIBMAUS2_PARALLEL_STDSPINLOCK_HPP

#include <libmaus2/LibMausConfig.hpp>
#include <libmaus2/exception/LibMausException.hpp>
#include <cerrno>
#include <atomic>

namespace libmaus2
{
	namespace parallel
	{
		struct ScopeStdSpinLock;
		struct ScopeStdSpinLockTryLock;

		struct StdSpinLock
                {
                	typedef StdSpinLock this_type;
                	typedef std::unique_ptr<this_type> unique_ptr_type;
                	typedef std::shared_ptr<this_type> shared_ptr_type;

			typedef ScopeStdSpinLock scope_lock_type;
			typedef ScopeStdSpinLockTryLock scope_try_lock_type;

			private:
			StdSpinLock & operator=(StdSpinLock const & O);
			StdSpinLock(StdSpinLock const & O);
                        std::atomic_flag flag;

			public:

                        StdSpinLock();
                        ~StdSpinLock();

                        void lock();
                        void unlock();

                        /**
                         * try to lock spin lock. returns true if locking was succesful, false if lock
                         * was already locked
                         **/
                        bool trylock();

                        /*
                         * try to lock spin lock. if succesful, lock is unlocked and return value is true,
                         * otherwise return value is false
                         */
                        bool tryLockUnlock();
                };

                struct ScopeStdSpinLock
                {
                	typedef ScopeStdSpinLock this_type;
                	typedef std::unique_ptr<this_type> unique_ptr_type;
                	typedef std::shared_ptr<this_type> shared_ptr_type;

                	StdSpinLock & spinlock;
                	bool locked;

                	void lock();
                	void unlock();

                	ScopeStdSpinLock(StdSpinLock & rspinlock, bool prelocked = false);
                	~ScopeStdSpinLock();
                };

                struct ScopeStdSpinLockTryLock
                {
                	typedef ScopeStdSpinLock this_type;
                	typedef std::unique_ptr<this_type> unique_ptr_type;
                	typedef std::shared_ptr<this_type> shared_ptr_type;

			StdSpinLock & mutex;
			bool locked;

			ScopeStdSpinLockTryLock(StdSpinLock & rmutex) : mutex(rmutex), locked(false)
			{
				locked = mutex.trylock();
			}
			~ScopeStdSpinLockTryLock()
			{
				if ( locked )
					mutex.unlock();
			}

			operator bool() const
			{
				return locked;
			}
		};

	}
}
#endif

