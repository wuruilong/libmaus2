/*
    libmaus2
    Copyright (C) 2009-2013 German Tischler
    Copyright (C) 2011-2013 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_STDBASICSPINLOCK_HPP)
#define LIBMAUS2_PARALLEL_STDBASICSPINLOCK_HPP

#include <libmaus2/LibMausConfig.hpp>
#include <libmaus2/exception/LibMausException.hpp>
#include <cerrno>
#include <atomic>

namespace libmaus2
{
	namespace parallel
	{
		struct ScopeStdBasicSpinLock;

		struct StdBasicSpinLock
                {
                	typedef StdBasicSpinLock this_type;
                	typedef std::unique_ptr<this_type> unique_ptr_type;
                	typedef std::shared_ptr<this_type> shared_ptr_type;

			typedef ScopeStdBasicSpinLock scope_lock_type;

			private:
			StdBasicSpinLock & operator=(StdBasicSpinLock const & O);
			StdBasicSpinLock(StdBasicSpinLock const & O);

			public:
                        std::atomic_flag flag;

                        StdBasicSpinLock() : flag()
                        {
                        	flag.clear();
                        }
                        ~StdBasicSpinLock()
                        {
                        }

                        void lock()
                        {
                        	while ( flag.test_and_set() )
                        	{

                        	}
                        }
                        void unlock()
                        {
                        	flag.clear();
                        }
                        /**
                         * try to lock spin lock. returns true if locking was succesful, false if lock
                         * was already locked
                         **/
                        bool trylock()
			{
				return (flag.test_and_set() == false);
                        }

                        /*
                         * try to lock spin lock. if succesful, lock is unlocked and return value is true,
                         * otherwise return value is false
                         */
                        bool tryLockUnlock()
                        {
                        	bool const r = trylock();
                        	if ( r )
                        		unlock();
				return r;
                        }
                };

                struct ScopeStdBasicSpinLock
                {
                	typedef ScopeStdBasicSpinLock this_type;
                	typedef std::unique_ptr<this_type> unique_ptr_type;
                	typedef std::shared_ptr<this_type> shared_ptr_type;

                	StdBasicSpinLock & spinlock;
                	bool locked;

                	void lock()
                	{
                		spinlock.lock();
                		locked = true;
                	}

                	void unlock()
                	{
                		spinlock.unlock();
                		locked = false;
                	}

                	ScopeStdBasicSpinLock(StdBasicSpinLock & rspinlock, bool prelocked = false)
                	: spinlock(rspinlock), locked(prelocked)
                	{
                		if ( ! prelocked )
                			lock();
                	}
                	~ScopeStdBasicSpinLock()
                	{
                		if ( locked )
	                		spinlock.unlock();
                	}
                };
	}
}
#endif

