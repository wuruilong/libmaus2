/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_INPUT_THREADPOOLINPUTBLOCK_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_INPUT_THREADPOOLINPUTBLOCK_HPP

#include <libmaus2/autoarray/AutoArray.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace input
			{
				struct ThreadPoolInputInfo;

				struct ThreadPoolInputBlock
				{
					typedef ThreadPoolInputBlock this_type;
					typedef std::unique_ptr<this_type> unique_ptr_type;
					typedef std::shared_ptr<this_type> shared_ptr_type;

					// block size
					std::atomic<uint64_t> blocksize;
					// size of putback area
					std::atomic<uint64_t> putback;
					// block memory (size putback + blocksize)
					libmaus2::autoarray::AutoArray<char> A;
					std::atomic<char *> pa;
					std::atomic<char *> pe;
					// start of memory block used
					std::atomic<char *> ca;
					// end of memory block used
					std::atomic<char *> ce;
					// id of block
					std::atomic<uint64_t> blockid;
					// true if this is the last input block
					std::atomic<bool> eof;
					// pointer to input info
					std::atomic<ThreadPoolInputInfo *> inputinfo;

					ThreadPoolInputBlock(uint64_t const rblocksize, uint64_t const rputback)
					: blocksize(rblocksize), putback(rputback), A(blocksize+putback,false), pa(A.begin()), pe(A.end()), ca(A.end()), ce(A.end()), blockid(0), eof(false)
					{

					}

					void setInputInfo(ThreadPoolInputInfo * r_inputinfo)
					{
						inputinfo.store(r_inputinfo);
					}

					ThreadPoolInputInfo * getInputInfo()
					{
						return inputinfo.load();
					}

					// increase putback buffer size
					void bumpPutBack()
					{
						// pointer offsets
						off_t const off_ca = pe-ca;
						off_t const off_ce = pe-ce;
						uint64_t const one = 1;
						uint64_t const oldsize = blocksize+putback;
						putback = std::max(one,putback<<1);

						// allocate new buffer
						libmaus2::autoarray::AutoArray<char> B(blocksize+putback,false);
						// copy data
						std::copy(pa.load(),pe.load(),B.end()-oldsize);

						// put new values
						A = B;
						pa = A.begin();
						pe = A.end();
						ca = A.end()-off_ca;
						ce = A.end()-off_ce;
					}

					bool operator<(ThreadPoolInputBlock const & O) const
					{
						return blockid.load() < O.blockid.load();
					}
				};
			}
		}
	}
}
#endif
