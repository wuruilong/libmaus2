/*
    libmaus2
    Copyright (C) 2021 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_CRAM_CRAMENCODINGWORKPACKAGEDISPATCHER_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_CRAM_CRAMENCODINGWORKPACKAGEDISPATCHER_HPP

#include <libmaus2/parallel/threadpool/cram/CramEncodingWorkPackageData.hpp>
#include <libmaus2/parallel/threadpool/ThreadPoolDispatcher.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace cram
			{
				struct CramEncodingWorkPackageDispatcher : public libmaus2::parallel::threadpool::ThreadPoolDispatcher
				{
					virtual void dispatch(
						libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::ThreadWorkPackage> package,
						libmaus2::parallel::threadpool::ThreadPool * /* pool */
					)
					{
						CramEncodingWorkPackageData & HP = dynamic_cast<CramEncodingWorkPackageData &>(*(package->data.load()));
						HP.dispatch();
					}
				};
			}
		}
	}
}
#endif
