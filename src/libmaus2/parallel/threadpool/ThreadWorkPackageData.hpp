/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADWORKPACKAGEDATA_HPP)
#define LIBMAUS2_PARALLEL_THREADWORKPACKAGEDATA_HPP

#include <string>
#include <typeinfo>
#include <typeindex>
#include <libmaus2/demangle/Demangle.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			struct ThreadWorkPackageData
			{
				virtual ~ThreadWorkPackageData() {}

				ThreadWorkPackageData & operator=(ThreadWorkPackageData const & O) = delete;

				virtual std::string getPackageTypeName() const
				{
					return libmaus2::demangle::Demangle::demangleName(typeid(*this).name());
				}
				virtual std::string toString() const
				{
					return getPackageTypeName();
				}

				std::type_index getTypeIndex() const
				{
					return std::type_index(typeid(*this));
				}
			};
		}
	}
}
#endif
