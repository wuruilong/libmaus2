/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_BGZF_READINFO_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_BGZF_READINFO_HPP

#include <libmaus2/parallel/AtomicPtrHeap.hpp>
#include <libmaus2/parallel/threadpool/input/ThreadPoolInputBlock.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace bgzf
			{
				struct ReadInfo
				{
					// input block queue
					AtomicPtrHeap<libmaus2::parallel::threadpool::input::ThreadPoolInputBlock> Q;
					// next block id to be processed
					std::atomic<uint64_t> next;
					std::atomic<uint64_t> absid;
					std::mutex lock;
					std::string putbackdata;

					private:
					ReadInfo(ReadInfo const & O) = delete;
					ReadInfo & operator=(ReadInfo const & O) = delete;

					public:
					ReadInfo() : Q(), next(0), absid(0), lock(), putbackdata() {}

					/**
					 * get next input block
					 **/
					bool getQNext(
						libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::input::ThreadPoolInputBlock> & blockptr
					)
					{
						std::lock_guard<std::mutex> slock(lock);

						if ( Q.pop(blockptr) )
						{
							if ( blockptr->blockid == next.load() )
								return true;
							else
								Q.push(blockptr);
						}

						return false;
					}
				};
			}
		}
	}
}
#endif
