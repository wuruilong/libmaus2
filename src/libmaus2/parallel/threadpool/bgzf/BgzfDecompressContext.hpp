/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFDECOMPRESSCONTEXT_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_BGZF_BGZFDECOMPRESSCONTEXT_HPP

#include <memory>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <atomic>
#include <libmaus2/exception/LibMausException.hpp>

#if defined(LIBMAUS2_HAVE_LIBDEFLATE)
struct libdeflate_decompressor;
#else
typedef struct z_stream_s zlib_z_stream;
#endif

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace bgzf
			{
				/* zlib decompression context */
				struct BgzfDecompressContext
				{
					typedef BgzfDecompressContext this_type;
					typedef std::shared_ptr<this_type> shared_ptr_type;

					private:
					BgzfDecompressContext & operator=(BgzfDecompressContext const & O) = delete;

					#if defined(LIBMAUS2_HAVE_LIBDEFLATE)
					std::shared_ptr<struct libdeflate_decompressor> decompressor;
					#else
					std::shared_ptr<zlib_z_stream> stream;
					#endif

					public:
					BgzfDecompressContext();
					~BgzfDecompressContext();

					void decompress(
						char * in,
						size_t n_in,
						char * out,
						size_t n_out
					);
					uint32_t crc32(char const * in, std::size_t const n);
				};
			}
		}
	}
}
#endif
