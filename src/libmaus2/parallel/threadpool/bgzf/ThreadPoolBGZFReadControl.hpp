/*
    libmaus2
    Copyright (C) 2020 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_BGZF_THREADPOOLBGZFREADCONTROL_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_BGZF_THREADPOOLBGZFREADCONTROL_HPP

#include <libmaus2/parallel/threadpool/bgzf/BgzfInputReadInterface.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace bgzf
			{
				struct ThreadPoolBGZFReadControl
				{
					libmaus2::parallel::threadpool::ThreadPool & TP;
					libmaus2::parallel::threadpool::bgzf::BgzfInputThreadControl BITC;
					libmaus2::parallel::threadpool::bgzf::BgzfInputReadInterface ITTRI;

					ThreadPoolBGZFReadControl(
						libmaus2::parallel::threadpool::ThreadPool & rTP,
						std::vector<std::string> const & rVfn,
						uint64_t const numblocks, // = 256,
						uint64_t const blocksize, // = 256*1024,
						uint64_t const putbacksize = 1024,
						uint64_t const baseprio = 64*1024,
						uint64_t const modprio = 256
					) : TP(rTP), BITC(TP,rVfn,numblocks,blocksize,putbacksize,baseprio,modprio), ITTRI(TP,BITC)
					{
						BITC.setReadCallback(std::vector<libmaus2::parallel::threadpool::input::InputThreadCallbackInterface *>(rVfn.size(),&ITTRI));
					}

					ThreadPoolBGZFReadControl(
						libmaus2::parallel::threadpool::ThreadPool & rTP,
						std::vector< std::shared_ptr<std::istream> > rVstr,
						uint64_t const numblocks, // = 64,
						uint64_t const blocksize, // = 256*1024,
						uint64_t const putbacksize = 1024,
						uint64_t const baseprio = 64*1024,
						uint64_t const modprio = 256
					) : TP(rTP), BITC(TP,rVstr,numblocks,blocksize,putbacksize,baseprio,modprio), ITTRI(TP,BITC)
					{
						BITC.setReadCallback(std::vector<libmaus2::parallel::threadpool::input::InputThreadCallbackInterface *>(rVstr.size(),&ITTRI));
					}

					void setBlockHandler(BgzfBlockInfoHandlerInterface * handler)
					{
						BITC.setBlockHandler(handler);
					}

					void start()
					{
						BITC.start();
					}
				};
			}
		}
	}
}
#endif
