/*
    libmaus2
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(LIBMAUS2_PARALLEL_THREADPOOL_BGZF_THREADPOOLBGZFDECOMPRESSCONTROL_HPP)
#define LIBMAUS2_PARALLEL_THREADPOOL_BGZF_THREADPOOLBGZFDECOMPRESSCONTROL_HPP

#include <libmaus2/parallel/threadpool/bgzf/BgzfDecompressPackageDataDispatcher.hpp>

namespace libmaus2
{
	namespace parallel
	{
		namespace threadpool
		{
			namespace bgzf
			{
				struct ThreadPoolBGZFDecompressControl
				{
					libmaus2::parallel::threadpool::ThreadPool & TP;
					libmaus2::parallel::threadpool::bgzf::ThreadPoolBGZFReadControl TPBRC;
					libmaus2::parallel::threadpool::bgzf::BgzfBlockInfoEnqueDecompressHandler BBITRH;

					ThreadPoolBGZFDecompressControl(
						libmaus2::parallel::threadpool::ThreadPool & rTP,
						std::vector<std::string> const & Vfn,
						BgzfDataDecompressedInterface * rdecompressedHandler,
						std::size_t const numinputblocks,
						std::size_t const inputblocksize,
						std::size_t const numdecompressedblocks
					) : TP(rTP), TPBRC(TP,Vfn,numinputblocks,inputblocksize), BBITRH(TP,TPBRC,Vfn.size(),numdecompressedblocks)
					{
						TPBRC.setBlockHandler(&BBITRH);
						TP.registerDispatcher<libmaus2::parallel::threadpool::bgzf::BgzfDecompressPackageData>(
							libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::ThreadPoolDispatcher>(
								new libmaus2::parallel::threadpool::bgzf::BgzfDecompressPackageDataDispatcher
							)
						);
						BBITRH.setDecompressedHandler(rdecompressedHandler);
					}

					ThreadPoolBGZFDecompressControl(
						libmaus2::parallel::threadpool::ThreadPool & rTP,
						std::vector< std::shared_ptr<std::istream> > rVstr,
						BgzfDataDecompressedInterface * rdecompressedHandler,
						std::size_t const numinputblocks,
						std::size_t const inputblocksize,
						std::size_t const numdecompressedblocks
					) : TP(rTP), TPBRC(TP,rVstr,numinputblocks,inputblocksize), BBITRH(TP,TPBRC,rVstr.size(),numdecompressedblocks)
					{
						TPBRC.setBlockHandler(&BBITRH);
						TP.registerDispatcher<libmaus2::parallel::threadpool::bgzf::BgzfDecompressPackageData>(
							libmaus2::util::shared_ptr<libmaus2::parallel::threadpool::ThreadPoolDispatcher>(
								new libmaus2::parallel::threadpool::bgzf::BgzfDecompressPackageDataDispatcher
							)
						);
						BBITRH.setDecompressedHandler(rdecompressedHandler);
					}

					void start()
					{
						TPBRC.start();
					}
				};
			}
		}
	}
}
#endif
